// Requirements
var fs = require('fs');
var Url = require('url');

/**
 * Main Downloader
 */
class Downloader {
  /**
   * Download a given url
   *
   * @see https://www.tomas-dvorak.cz/posts/nodejs-request-without-dependencies/
   * @see http://www.hacksparrow.com/using-node-js-to-download-files.html
   *
   * @param {string} url Url of file to Download
   * @param {string} dest File destination
   * @param {string} authToken Authentication token
   *
   * @return {Promise} promise Promise that resolve with boolean :
   *                           - false if file already exists
   *                           - true if file was downloaded
   */
  static download(url, dest, authToken) {
    return new Promise((resolve, reject) => {
      // Check if file already exists
      if (fs.existsSync(dest)) {
        return resolve(false);
      }

      // Initialize
      const lib = url.startsWith('https') ? require('https') : require('http');
      const urlParts = Url.parse(url);
      var options = {
        host: urlParts.hostname,
        port: (url.startsWith('https') ? 443 : 80),
        path: urlParts.pathname,
        headers: {
          'Authorization': 'Bearer ' + authToken
        }
      };

      // Create file
      var file = fs.createWriteStream(dest);
      const request = lib.get(options, (response) => {
        // handle http errors
        if (response.statusCode < 200 || response.statusCode > 299) {
          reject(new Error('Failed to load page, status code: ' + response.statusCode));
        }
        response.on('data', (data) => { file.write(data) });
        response.on('end', () => {
          file.end()
          resolve(true);
        });
      });

      // Handle connection errors of the request
      request.on('error', (err) => reject(err))
    });
  }
}

export default Downloader;
