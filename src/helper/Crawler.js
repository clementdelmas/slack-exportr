// Requirements
var colors = require('colors');

// Slack Requirements
var slack = require('slack');
import Downloader from './Downloader';

/**
 * Main Crawler
 */
class Crawler {
  /**
   * Crawl Slack pages
   *
   * @param {Object} slackConfig Configuration of Slack to crawl
   *
   * @return {void}
   */
  static crawlSlackPages(slackConfig) {
    return new Promise((resolve) => {
      slack.files.list({ count: 1, token: slackConfig.token }, (err, data) => {
        // Check error
        if (err !== null) {
          throw new Error("[Crawler] Can't get Slack files");
        }

        // Initialize
        let crawlPromises = [];
        let totalPages = Math.ceil((data.paging.total / slackConfig.count));

        // Crawl all pages
        for (var page = 1; page <= totalPages; page++) {
          crawlPromises.push(this.crawlSlackFiles(slackConfig, page));
        }

        Promise.all(crawlPromises).then(() => {
          resolve();
        });
      });
    });
  }

  /**
   * Crawl files of a given Slack
   *
   * @param {Object} slackConfig Configuration of Slack to crawl
   *
   * @return {void}
   */
  static crawlSlackFiles(slackConfig, page = 1) {
    return new Promise((resolveCrawl) => {
      slack.files.list({
        token: slackConfig.token,
        count: slackConfig.count || 10,
        page: page
      }, function(err, data) {
        // Check error
        if (err !== null) {
          throw new Error("[Crawler] Can't get Slack files");
        }

        // Check for empty list
        if (data.files.length === 0) {
          throw new Error("[Crawler] No items found");
        }

        // Initialize
        var downloadPromises = [];
        var notDownloadableFiles = [];

        // Loop on files
        data.files.forEach(function(file) {
          // Initialize
          let newFileName = file.created + ' - ' + file.title + '.' + file.filetype;
          let dest = slackConfig.folder + newFileName;

          // Check for url
          if (!file.url_private_download) {
            // Don't download file
            notDownloadableFiles.push(file);
            return false;
          }

          // Do add promise
          let downloadPromise = Downloader.download(file.url_private_download, dest, slackConfig.token);
          downloadPromise.then((status) => {
            if (slackConfig.logFiles) {
              console.log((status ? '[Downloaded]'.green : '[Exists]'.yellow), dest.grey);
            }
          });
          downloadPromise.catch((err) => {
            console.log('[Error]'.red, err);
          })
          downloadPromises.push(downloadPromise);
        });

        // Let's download everything
        Promise.all(downloadPromises).then(() => {
          // Download complete!
          console.log(colors.green('[' + slackConfig.name + '] Page ' + page + ' - Download complete'));

          // Check for not downladable files
          if (notDownloadableFiles.length) {
            console.log('[' + slackConfig.name + '] Not downloaded files:');
            notDownloadableFiles.forEach(function(file) {
              console.log(' - ' + file.url_private);
            });
          }

          resolveCrawl();
        });
      });
    });
  }
}

export default Crawler;
