// Initialize
require('colors');
var fs = require('fs');

// Slack requirements
import Crawler from './src/helper/Crawler';
var slacksConfig = JSON.parse(fs.readFileSync('./config/slacks.json', 'utf8'));

// Check slacksConfig
if (slacksConfig.length === 0) {
  throw new Error('Slacks configuration is empty');
}

// Initialize
var exportPromises = [];

// Loop on Slacks configuration
for (var slackSlug in slacksConfig) {
  // Check config
  if (!slacksConfig[slackSlug]) {
    continue;
  }

  exportPromises.push(new Promise((resolve) => {
    // Initialize
    var slackConfig = slacksConfig[slackSlug];

    // Check folder
    if (!fs.existsSync(slackConfig.folder)) {
      fs.mkdirSync(slackConfig.folder);
    }

    // Let's crawl!
    console.log('');
    console.log('[' + slackConfig.name.green + ']' + " Let's crawl!".grey);
    Crawler.crawlSlackPages(slackConfig)
      .then((data) => {
        resolve(data);
      });
  }));
}

Promise.all(exportPromises).then(() => {
  console.log('[' + 'SlackExportr'.green + ']' + ' Export done'.grey);
});
