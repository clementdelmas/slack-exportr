# Slack Exporter

Slack Exporter is about exporting all accessible Slack files of your Slacks.
Because you already can export all your conversations, but not your files.

## How to use it

1. Login to Slack and generate Slack API tokens, you'll need them to export files.
2. Copy the `./config/slacks.json.dist` file to `./config/slacks.json` and update it
with your Slacks configurations.
3. That's it. You can run the project.

The main command to run it is:

    $ npm run start
